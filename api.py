
from django.http import JsonResponse


def custom404(request):
    return JsonResponse({
        'ErrorCode': 404,
        'ErrorDescription': 'El recruso solicitado no se encuentra'
    })