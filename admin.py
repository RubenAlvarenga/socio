# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from socio.models import socio

admin.site.register(socio)
