# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class socio(models.Model):
    ESTADO_CHOICES = (
        ('A','ACTIVO'),
        ('I','INACTIVO'),
        ('B','BLOQUEADO')
    )    
    matricula = models.DecimalField(max_digits=9 ,decimal_places=0, primary_key=True, verbose_name='Matricula')
    nombres = models.CharField(max_length=40, verbose_name='Nombres')
    apellidos =models.CharField(max_length=40, verbose_name='Apellidos')
    estado =models.CharField(max_length=1, choices=ESTADO_CHOICES, verbose_name='Estado', default='A')
    cedula =models.CharField(max_length=8, verbose_name='Cedula', unique=True)
    nacimiento =models.DateField( verbose_name='Nacimiento', blank=True, null=True)

    class Meta:
        ordering = ['matricula']
        verbose_name = 'Socio'
        verbose_name_plural = 'Socios'

    def __unicode__(self):
        return '%s %s, %s' % (self.matricula, self.apellidos, self.nombres )
